angular.module('rtcapp')
    .directive('autoDim', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {

                function resizeVideo() {
                    var widthRow = $(element[0]).width();
                    var selfWidth = widthRow * 0.2;

                    $(element[0]).height((widthRow / 16) * 9);
                    var selfVideo = $(element[0]).find('#selfVideo')[0];
                    $(selfVideo).width(selfWidth).height((selfWidth / 16) * 9);
                }

                scope.$on('onResizeVideo', function(e, d) {
                    console.log('onResizeVideo');
                    resizeVideo();
                });

                resizeVideo();
            }
        }
    })
    .directive('growHeight', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                function resizeVideo() {
                    var height = $('#panelLeft').height() - $('#panelRightTop').height();
                    $(element[0]).height(height);
                }

                resizeVideo();
                scope.$emit('onUpdateScroller');
            }
        }
    })
    .directive('nanoScroller', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {

                function initScroller() {
                    setTimeout(function() {
                        $(element[0]).nanoScroller();
                    }, 1000);
                }

                initScroller();

                scope.$on('onUpdateScroller', function(event, data) {
                    initScroller();
                });
            }
        }
    })
    .controller('mainCtrl', ['$rootScope', '$scope', '$state', '$stateParams', function($rootScope, $scope, $state, $stateParams) {
        // check state
        $scope.$on('onCheckAuth', function(event, data) {
            if (!$scope.user.easyrtcid) {
                $state.go('join');
            }
        });

        var _init = function() {
            $scope.user = {
                connection: []
            };
            // test data
            // {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }, {
            //     easyrtcid: 'faksdfhasdhf',
            //     name: 'sdkfjhakljsdhf'
            // }
            $scope.users = [];
            $scope.initSource = false;
            $scope.callerStream = undefined;
            $scope.medium = true;
            $scope.isFullScreen = false;

            // *********************************************
            easyrtc.setStreamAcceptor(function(callerEasyrtcid, stream) {
                console.debug('on setStreamAcceptor');

                if ($('#callerVideo').length > 0) {
                    easyrtc.setVideoObjectSrc($('#callerVideo')[0], stream);
                    easyrtc.setVideoObjectSrc($('#selfVideo')[0], easyrtc.getLocalStream());
                } else {
                    $scope.callerStream = stream;
                }

                $scope.$apply();
            });

            easyrtc.setOnStreamClosed(function(callerEasyrtcid) {
                easyrtc.clearMediaStream($('#callerVideo')[0]);
                // easyrtc.clearMediaStream($('#selfVideo')[0]);
            });
        }

        _init();

        // use with full page
        $scope.connect = function(form) {
            if (form.$valid) {
                if (easyrtc.isNameValid($scope.user.name)) {
                    easyrtc.setUsername($scope.user.name);
                }

                easyrtc.connect("Company_Chat_Line", function(easyrtcid) {
                    $scope.user.easyrtcid = easyrtcid;
                    $scope.$apply();

                    _initEasyRtc();
                    $state.go('full');
                }, function(errorCode, errText) {
                    console.log('error connect to server: ', errText);
                });
            }
        };

        // test auto connect
        // $scope.connect({ $valid: true });

        var _initEasyRtc = function() {
            easyrtc.setPeerListener(function(easyrtcid, msgType, content) {
                if (msgType === 'chat:videoconnected') {
                    $scope.user.connection.push(easyrtcid);
                } else if (msgType === 'chat:videodisconnected') {
                    _spliceCallList(easyrtcid);
                }

                $scope.$apply();
            });

            easyrtc.setRoomOccupantListener(function(roomName, occupants) {
                $scope.users = [];

                for (easyrtcid in occupants) {
                    $scope.users.push({
                        easyrtcid: easyrtcid,
                        name: easyrtc.idToName(easyrtcid),
                    });
                }

                $scope.$apply();
            });

            easyrtc.setAcceptChecker(function(callerId, acceptor) {
                var answer = false;
                if (easyrtc.getConnectionCount() > 0) {
                    answer = confirm("Drop current call and accept new from " + callerId + " ?");
                } else {
                    answer = confirm("Accept incoming call from " + callerId + " ?");
                }
                if (answer && easyrtc.getConnectionCount() > 0) {
                    easyrtc.hangupAll();
                }
                if (answer) {
                    $scope.hangupAll();
                }
                acceptor(answer);
            });
        }

        $scope.initMediaSource = function() {
            console.log('initMediaSource');
            easyrtc.enableAudio(true);
            easyrtc.enableVideo(true);
            easyrtc.setVideoDims(1280, 720);
            // easyrtc.setVideoDims(854, 480);
            // easyrtc.setVideoDims(200, 150);

            easyrtc.initMediaSource(
                function() {
                    easyrtc.setVideoObjectSrc($('#callerVideo')[0], $scope.callerStream);
                    easyrtc.setVideoObjectSrc($('#selfVideo')[0], easyrtc.getLocalStream());
                },
                function(errorCode, errText) {
                    console.log(errText);
                }
            );
        }

        $scope.call = function(easyrtcid) {
            if (easyrtc.getConnectionCount() > 0) {
                var answer = confirm('Drop current call to call ' + easyrtc.idToName(easyrtcid) + '?');
                if (!answer) {
                    return;
                }
            }

            if (!$scope.initSource) {
                $scope.initSource = true;
                $scope.initMediaSource();
            }

            $scope.hangupAll();

            easyrtc.call(
                easyrtcid,
                function(otherCaller, mediaType) { // success callback
                    // mediaType is audiovideo or datachanel (connection)
                    console.log("completed call to " + easyrtc.idToName(otherCaller) + '#' +
                        otherCaller);
                    console.log('media type ', mediaType);
                },
                function(errorCode, errorText) { // failure callback
                    console.log("err:" + errorText);
                },
                function(accepted, ortherUser) { // accepted or rejected callback
                    console.log((accepted ? "accepted" : "rejected") + " by " + easyrtc.idToName(ortherUser) + '#' +
                        ortherUser);

                    if (accepted) {
                        easyrtc.sendDataWS(easyrtcid, "chat:videoconnected", null);
                        $scope.user.connection.push(easyrtcid);
                    }
                }
            );
        }

        $scope.hasConnect = function(easyrtcid) {
            var hasConnect = false;
            $.each($scope.user.connection, function(index, value) {
                if (value === easyrtcid) {
                    hasConnect = true;
                    return false;
                }
            });

            return hasConnect;
        }

        $scope.hangup = function(easyrtcid) {
            easyrtc.hangup(easyrtcid);
            easyrtc.sendDataWS(easyrtcid, "chat:videodisconnected", null);

            _spliceCallList(easyrtcid);
        }

        var _spliceCallList = function(easyrtcid) {
            var index = $scope.user.connection.indexOf(easyrtcid);
            if (index > -1) {
                $scope.user.connection.splice(index, 1);
            }
        };

        $scope.hangupAll = function() {
            easyrtc.hangupAll();

            $.each($scope.user.connection, function(index, value) {
                easyrtc.sendDataWS(value, "chat:videodisconnected", null);
            });
            $scope.user.connection = [];
        }

        $scope.disconnect = function() {
            easyrtc.disconnect();
        }

        $scope.toggleFullScreen = function() {
            if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
                if (document.documentElement.requestFullscreen) {
                    document.documentElement.requestFullscreen();
                } else if (document.documentElement.mozRequestFullScreen) {
                    document.documentElement.mozRequestFullScreen();
                } else if (document.documentElement.webkitRequestFullscreen) {
                    document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                }
            } else {
                if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                }
            }
        }

        $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
            if (document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement === null) {
                $scope.isFullScreen = false;
            } else {
                $scope.isFullScreen = true;
            }

            if ($scope.isFullScreen) {
                $(".video-container").addClass('fullscreen');
            } else {
                $(".video-container").removeClass('fullscreen');
            }

            $scope.$apply();
            $scope.$broadcast('onResizeVideo');
        })

    }]);