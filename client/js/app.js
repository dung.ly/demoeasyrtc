angular.module('rtcapp', [
    'ui.router',
    'ngMessages'
]);

function onReady() {
    angular.bootstrap(document, ['rtcapp'], {});
}

angular.element(document).ready(onReady);
