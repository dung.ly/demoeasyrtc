angular.module('rtcapp')
    .config(function($urlRouterProvider, $stateProvider, $locationProvider) {

        $stateProvider
            .state('join', {
                url: '/join',
                templateUrl: '/pages/join.html'
            })
            // .state('list', {
            //     url: '/list',
            //     templateUrl: '/pages/list.html'
            // })
            // .state('call', {
            //     url: '/call/:easyrtcid',
            //     templateUrl: '/pages/call.html'
            // })
            .state('full', {
                url: '/full',
                templateUrl: '/pages/full.html'
            });

        $urlRouterProvider.otherwise('/join');
    })
    .run(function($rootScope, $location) {
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            if (toState.name === 'full') {
                $rootScope.$broadcast('onCheckAuth', 'Checking authentication');
            }
        });
    });
